import 'package:flutter/material.dart';
import '../wedgets/create_transaction.dart';
import '../wedgets/transaction_list.dart';
import '../models/Transaction.dart';

class TransactionManager extends StatefulWidget {
  @override
  _TransactionManagerState createState() => _TransactionManagerState();
}

class _TransactionManagerState extends State<TransactionManager> {
   final List<Transaction> _transactions = [
    Transaction(
        id: 't1', title: 'New shoes', amount: 3500.0, date: DateTime.now()),
    Transaction(
        id: 't2', title: 'New Iphone', amount: 200000.0, date: DateTime.now()),
    Transaction(
        id: 't3', title: 'New car', amount: 1000000.0, date: DateTime.now()),
    Transaction(
        id: 't4', title: 'New soap', amount: 200.0, date: DateTime.now()),
    Transaction(
        id: 't5', title: 'New laptop', amount: 300000.0, date: DateTime.now()),
  ];
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        CreateTransaction(),
        TransactionList(_transactions),
      ],
    );
  }
}