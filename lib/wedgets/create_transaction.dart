import 'package:flutter/material.dart';

class CreateTransaction extends StatelessWidget {
  final titleController=TextEditingController();
  final amountController=TextEditingController();
  @override
  Widget build(BuildContext context) {
    return  Card(
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: titleController,
                    decoration: InputDecoration(labelText: 'Title'),
                  ),
                  TextField(
                    controller: amountController,
                    decoration: InputDecoration(labelText: 'Amount'),
                  ),
                  Align(
                    alignment: Alignment.topRight,
                    child: FlatButton(
                      textColor: Colors.purple,
                      onPressed: () {
                      print(titleController.text);
                      },
                      child: Text('Add Transaction'),
                    ),
                  )
                ],
              ),
            ),
          );
  }
}